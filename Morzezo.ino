#include <Arduino.h>

#ifndef LED_BUILTIN
#define LED_BUILTIN 1
#endif

#include <string>

using namespace std;

#define EGYSEG 200

struct MorseJel
{
    char betu;
    const char* code;
};

enum PlayState
{
    JEL,
    JELSZUNET,
    BETUSZUNET,
    SZOSZUNET,
    MONDATSZUNET,
    UZENETSZUNET,
    IDLE
};

MorseJel jelek[] = {
    {
        '*',
        "-..-"
    },
    {
        'a',
        ".-"
    },
    {
        's',
        "..."
    },
    {
        'o',
        "---"
    },
    {
        0,
        nullptr
    }
};

// Return first symbol if not found
MorseJel* FindJel(char c)
{
    for(int i = 0; jelek[i].betu != 0; i++)
    {
        if (c == jelek[i].betu)
            return &jelek[i];
    }

    return &jelek[0];
}

struct Morzezo
{
    public:
        void SetWord(const string& word)
        {
            // TODO: tolower
            message = word;
            charpos = 0;
            symbolpos = -1;
            SetState(IDLE);
        }
        void loop();

    private:
        
        PlayState stateMachine;
        string message;
        int charpos;
        char currChar;
        MorseJel* currJel;
        int symbolpos;

        unsigned long timeStamp;

        void SetState(PlayState st)
        {
            Serial.printf("Set state:%d\n",st);

            stateMachine = st;
            timeStamp = millis();
        }
        bool CharPlaying()
        {
            return symbolpos >= 0;
        }
        void CharStartPlay();
        bool CharPlayProgress();
        void CharNext();
};

void lampa(bool on)
{
    digitalWrite(LED_BUILTIN, on ? LOW : HIGH);
}

bool Morzezo::CharPlayProgress()
{
    auto duration = millis() - timeStamp;
    switch(stateMachine)
    {
    case SZOSZUNET:
        if (duration < EGYSEG * 7)
            return true;
        break;
    case JEL:
    {
        if (duration < EGYSEG * (
            currJel->code[symbolpos] == '.' ? 1 : 3))
            return true;
        SetState(JELSZUNET);
        lampa(false);
    }
    break;
    case JELSZUNET:
        if (duration < EGYSEG * 1)
            return true;
        symbolpos++;
        if (currJel->code[symbolpos] == 0)
            SetState(BETUSZUNET);
        else
        {
            SetState(JEL);
            lampa(true);
        }
        break;
    case BETUSZUNET:
        if (duration < EGYSEG * 3)
            return true;
        charpos++;
        if (charpos >= message.length())
            SetState(UZENETSZUNET);
        else
            CharStartPlay();
        break;
    }
}

void Morzezo::CharStartPlay()
{
    symbolpos = 0;
    currChar = message[charpos];
    currJel = FindJel(currChar);

    Serial.printf("CharStartplay %d\n", currChar);
    if (currChar == ' ')
    {
        lampa(false);
        SetState(SZOSZUNET);
    }
    else
    {
        Serial.println("Jel ");
        lampa(true);
        SetState(JEL);
    }
}

void Morzezo::CharNext()
{
    charpos++;
    if (charpos >= message.length())
    {
        SetState(UZENETSZUNET);
        symbolpos = -1;
        charpos = 0;
    }
}

void Morzezo::loop()
{
    if (message.length() == 0)
        return;

    // TODO: millis - timestamp should be checked here
    
    if (stateMachine == UZENETSZUNET)
    {
        if (millis() - timeStamp < 15*EGYSEG)
        return;
        SetState(IDLE);
    }
    
    if (!CharPlaying())
    {
        CharStartPlay();
        return;
    }

    if (!CharPlayProgress())
        CharNext();
}

Morzezo morze;

void setup()
{
    Serial.begin(115200);

    pinMode(LED_BUILTIN, OUTPUT);
    
    morze.SetWord("sos");
    Serial.println("Started");
}

void loop()
{
    morze.loop();
}
